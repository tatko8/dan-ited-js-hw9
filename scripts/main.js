/*

Питання 1. Опишіть, як можна створити новий HTML тег на сторінці.

Відповідь 1. Через document.createElement('назва тегу')

Питання 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

Відповідь 2. Перший параметр - позиція елемента відносно того елемента, який визвав метод. 
            Має наступні варіанти: 
            - 'beforebegin' - до відкриваючого тегу елементу.
            - 'afterbegin' - після відкриваючого тегу елементу.
            - 'beforeend' - перед закриваючим тегом елемента.
            - 'afterend' - після закриваючого тегу елемента.

Питання 3. Як можна видалити елемент зі сторінки?

Відповідь 3. Методом remove()

*/

"use strict"; //strict mode is a way to introduce better error-checking into your code 

// let array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let array = ["1", "2", "3", "sea", "user", 23];

let body = document.body;
let list = [];
 
array.map(function(array, body) {
    list = document.write('<li>' + array + '</li>');
});

console.log(body);
